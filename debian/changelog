loqui (0.7.0-2) unstable; urgency=medium

  * Upload to unstable 

 -- Hideki Yamane <henrich@debian.org>  Sun, 15 Aug 2021 12:24:56 +0900

loqui (0.7.0-1) experimental; urgency=medium

  * New upstream release (Closes: #967600)
  * debian/control
    - Update Build-Depends: libgtk-3-dev for gtk3
    - Drop unnecessary glib2.0 dependency
    - Add new dependency gob2
    - Set Standards-Version: 4.5.1
    - Move upstream Homepage to https://github.com/sunnyone/loqui
    - Add Rules-Requires-Root: no
  * debian/patches
    - Add 0001-Add-missing-Makefile.in.patch
  * debian/rules
    - Generate Makefile.am files from .m4 files
  * debian/copyright
    - Update Source URL
    - update copyright year
  * debian/watch
    - Move to github.com
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Add debian/gbp.conf

 -- Hideki Yamane <henrich@debian.org>  Wed, 26 May 2021 00:06:34 +0900

loqui (0.6.4-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove trailing whitespaces

  [ Hideki Yamane ]
  * debian/control
    - Set Standards-Version: 4.5.0
    - Drop debhelper and set debhelper-compat (= 13)
  * Drop debian/compat
  * Drop debian/.gitlab-ci.yml and use debian/salsa-ci.yml instead

 -- Hideki Yamane <henrich@debian.org>  Wed, 24 Jun 2020 20:15:51 +0900

loqui (0.6.4-3) unstable; urgency=medium

  * debian/control
    - move Vcs-* to salsa.debian.org
    - set Build-depends: debhelper (>= 11)
    - set Standards-Version: 4.1.3
  * debian/compat
    - set 11
  * debian/watch
    - update to version 4
  * debian/copyright
    - update copyright year and use https
  * debian/{install,loqui.xpm}
    - menu system was deprecated, so remove it

 -- Hideki Yamane <henrich@debian.org>  Mon, 12 Feb 2018 21:32:42 +0900

loqui (0.6.4-2) unstable; urgency=medium

  * debian/compat
    - set 10
  * debian/control
    - set "Build-Depends: debhelper (>= 10)" and drop unnecessary dependency
      as dh-autoreconf, autoconf, automake and libtool
    - set "Standards-Version: 4.0.0"
    - use https for Vcs-*
  * debian/rules
    - simplify rules with dh10
  * debian/loqui.lintian-overrides
    - dropped: lintian "unused-override" warning

 -- Hideki Yamane <henrich@debian.org>  Sat, 24 Jun 2017 23:48:00 +0900

loqui (0.6.4-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Fri, 18 Dec 2015 22:06:12 +0900

loqui (0.6.3-2) unstable; urgency=medium

  * remove debian/menu to fix "command-in-menu-file-and-desktop-file"

 -- Hideki Yamane <henrich@debian.org>  Sat, 05 Dec 2015 17:43:41 +0900

loqui (0.6.3-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Sun, 15 Nov 2015 23:49:13 +0900

loqui (0.6.2-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Sat, 14 Nov 2015 16:14:20 +0900

loqui (0.6.1-1) unstable; urgency=medium

  * Imported Upstream version 0.6.1
  * debian/rules
    - use dh-autoreconf, instead of autotools-dev
  * debian/control
    - adjust Build-Depends: as above
    - set "Standards-Version: 3.9.6"
    - update Vcs-Browser

 -- Hideki Yamane <henrich@debian.org>  Sat, 04 Jul 2015 21:12:05 +0900

loqui (0.5.5-3) unstable; urgency=medium

  * debian/control
    - set "Standards-Version: 3.9.5"
    - add Vcs-* field

 -- Hideki Yamane <henrich@debian.org>  Sun, 08 Jun 2014 12:05:49 +0900

loqui (0.5.5-2) unstable; urgency=low

  * Upload to unstable

 -- Hideki Yamane <henrich@debian.org>  Mon, 06 May 2013 18:59:59 +0900

loqui (0.5.5-1) experimental; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Wed, 27 Mar 2013 21:07:54 +0900

loqui (0.5.4-1) unstable; urgency=low

  * New upstream release
  * debian/patches: drop it since merged to upstream.

 -- Hideki Yamane <henrich@debian.org>  Mon, 28 Jan 2013 06:33:35 +0900

loqui (0.5.3-5) unstable; urgency=low

  * debian/rules
    - use autotools-dev
  * debian/control
    - set "Standards-Version: 3.9.4"
  * debian/loqui-lintian.overrides
    - override "loqui: hardening-no-fortify-functions usr/bin/loqui"
      since it would be false-positive

 -- Hideki Yamane <henrich@debian.org>  Sun, 13 Jan 2013 21:21:23 +0900

loqui (0.5.3-4) unstable; urgency=low

  * debian/rules
    - enable parallel build
  * debian/patches
    - add descriptions

 -- Hideki Yamane <henrich@debian.org>  Thu, 05 Jul 2012 05:30:14 +0900

loqui (0.5.3-3) unstable; urgency=low

  * debian/copyright
    - update its Format: field to fix Lintian warning,
      "unversioned-copyright-format-uri"

 -- Hideki Yamane <henrich@debian.org>  Tue, 05 Jun 2012 12:49:13 +0900

loqui (0.5.3-2) unstable; urgency=low

  * Fix "CPPFLAGS hardening flags missing" (Closes: #674721)
    Thanks to Simon Ruderich <simon@ruderich.org>

 -- Hideki Yamane <henrich@debian.org>  Tue, 29 May 2012 07:46:33 +0900

loqui (0.5.3-1) unstable; urgency=low

  * New upstream release
  * debian/rules
    - use "DEB_BUILD_MAINT_OPTIONS = hardening=+all"
      (however, DEB_BUILD_HARDENING_FORTIFY doesn't work well now).
    - add override_dh_auto_configure target to generate patched Makefiles
  * debian/control
    - set "Standards-Version: 3.9.3" with no change
    - set "Build-Depends: debhelper (>= 9)"
    - add autoconf, automake and libtool to Build-Depends
    - fix description, GTK"+"
  * debian/watch
    - fix to work

 -- Hideki Yamane <henrich@debian.org>  Thu, 24 May 2012 22:10:23 +0900

loqui (0.5.2-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Sat, 19 Nov 2011 00:45:27 +0900

loqui (0.5.1-2) unstable; urgency=low

  * debian/rules
    - add "DEB_BUILD_MAINT_OPTIONS := hardening=+pie,+bindno" to harden it
  * debian/compat
    - set 9
  * debian/control
    - set "debhelper (>= 8.9.0)"

 -- Hideki Yamane <henrich@debian.org>  Sat, 22 Oct 2011 22:45:34 +0900

loqui (0.5.1-1) unstable; urgency=low

  * New upstream release (Closes: #645269)
  * debian/rules
    - simplified as default dh7 rules
  * debian/control
    - set "Standards-Version: 3.9.2"
    - set "Maintainer: Hideki Yamane <henrich@debian.org>" and
      "Uploaders: Yoichi Imai <sunnyone41@gmail.com>"
    - set "Build-Depends: debhelper (>> 7.0.50~)" to use dh7
    - update build dependency, "libglib2.0-dev (>= 2.22.0)" and drop
      libgnet-dev
    - add Homepage: field
    - fix "W: loqui: description-starts-with-leading-spaces"
    - improve description
  * debian/copyright
    - update information, some files are licensed under LGPL-2+
    - convert to DEP5 style
  * debian/menu
    - rewrite it to fix lintian warning and for improvement
  * add debian/{install,loqui.xpm} for menu entry
  * add debian/compat
  * add debian/watch
  * remove unnecessary debian/dirs
  * use source format "3.0 (quilt)"

 -- Hideki Yamane <henrich@debian.org>  Fri, 14 Oct 2011 06:50:47 +0900

loqui (0.4.4-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Thu, 13 Oct 2005 20:57:31 +0900

loqui (0.4.3-1) unstable; urgency=low

  * new upstream release

 -- yoichi <yoichi@silver-forest.com>  Wed, 12 Oct 2005 18:39:22 +0900

loqui (0.4.2-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Thu, 25 Aug 2005 02:31:04 +0900

loqui (0.4.1-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Mon, 18 Jul 2005 03:31:38 +0900

loqui (0.4.0pre4-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Sun,  3 Jul 2005 20:02:33 +0900

loqui (0.4.0pre3-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Sat,  2 Jul 2005 16:08:33 +0900

loqui (0.4.0pre2-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Wed, 25 May 2005 20:25:21 +0900

loqui (0.4.0pre1-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Sun, 15 May 2005 19:06:14 +0900

loqui (0.3.9-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Wed, 22 Sep 2004 01:20:18 +0900

loqui (0.3.8-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Thu, 26 Aug 2004 01:39:14 +0900

loqui (0.3.7-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Sat, 19 Jun 2004 02:57:57 +0900

loqui (0.3.6-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Tue, 15 Jun 2004 23:54:36 +0900

loqui (0.3.5-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Wed, 26 May 2004 21:03:39 +0900

loqui (0.3.4-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Thu, 13 May 2004 22:32:27 +0900

loqui (0.3.3-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Tue, 04 May 2004 14:50:38 +0900

loqui (0.3.2-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Sat, 01 May 2004 20:44:01 +0900

loqui (0.3.1-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Fri, 16 Apr 2004 21:29:10 +0900

loqui (0.3.0pre4-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Sat, 10 Apr 2004 00:02:50 +0900

loqui (0.3.0pre3-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Fri, 09 Apr 2004 17:51:03 +0900

loqui (0.3.0pre2-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Mon, 05 Apr 2004 22:55:18 +0900

loqui (0.3.0pre1-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Sun, 04 Apr 2004 02:04:06 +0900

loqui (0.2.5-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Sun, 07 Mar 2004 03:52:53 +0900

loqui (0.2.4-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Thu, 26 Feb 2004 01:52:10 +0900

loqui (0.2.3-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Sun, 08 Feb 2004 23:47:27 +0900

loqui (0.2.2-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Thu, 22 Jan 2004 15:38:46 +0900

loqui (0.2.1-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Mon, 19 Jan 2004 00:50:59 +0900

loqui (0.2.0-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Sun, 18 Jan 2004 19:34:21 +0900

loqui (0.1.20-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Sun, 21 Dec 2003 03:28:31 +0900

loqui (0.1.19-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Thu, 11 Sep 2003 03:29:25 +0900

loqui (0.1.18-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Tue, 01 Jul 2003 21:16:29 +0900

loqui (0.1.17-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Mon, 16 Jun 2003 03:32:40 +0900

loqui (0.1.16-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Sat, 07 Jun 2003 21:56:41 +0900

loqui (0.1.15-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Thu, 29 May 2003 20:17:20 +0900

loqui (0.1.14-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Thu, 17 Apr 2003 21:33:53 +0900

loqui (0.1.13-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Wed, 26 Mar 2003 12:42:25 +0900

loqui (0.1.12-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Mon, 24 Mar 2003 14:44:55 +0900

loqui (0.1.11-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Tue, 18 Mar 2003 01:13:54 +0900

loqui (0.1.10-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Sun, 16 Mar 2003 17:41:11 +0900

loqui (0.1.9-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Sun, 16 Mar 2003 14:48:44 +0900

loqui (0.1.8-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Tue, 25 Feb 2003 00:37:29 +0900

loqui (0.1.7-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Sun, 23 Feb 2003 22:43:53 +0900

loqui (0.1.6-1) unstable; urgency=low

  * new upstream release.

 -- Yoichi Imai <yoichi@silver-forest.com>  Wed, 19 Feb 2003 23:05:24 +0900

loqui (0.1.5-1) unstable; urgency=low

  * new upstream release.

 -- Yoichi Imai <yoichi@silver-forest.com>  Wed, 19 Feb 2003 15:46:52 +0900

loqui (0.1.4-1) unstable; urgency=low

  * new upstream release.

 -- Yoichi Imai <yoichi@silver-forest.com>  Sun, 16 Feb 2003 14:27:15 +0900

loqui (0.1.3-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Wed, 12 Feb 2003 02:42:04 +0900

loqui (0.1.2-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Tue, 11 Feb 2003 00:04:55 +0900

loqui (0.1.1-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Sat,  8 Feb 2003 23:06:58 +0900

loqui (0.1.0-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Mon, 27 Jan 2003 00:27:24 +0900

loqui (0.0.9-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Thu, 23 Jan 2003 20:44:47 +0900

loqui (0.0.8-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Sun, 19 Jan 2003 21:29:09 +0900

loqui (0.0.7-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Sun, 19 Jan 2003 00:07:11 +0900

loqui (0.0.6-1) unstable; urgency=low

  * new upstream release

 -- Yoichi Imai <yoichi@silver-forest.com>  Sun, 12 Jan 2003 23:41:19 +0900

loqui (0.0.5-2) unstable; urgency=low

  * fixed undocumented

 -- Yoichi Imai <yoichi@silver-forest.com>  Fri, 10 Jan 2003 21:28:08 +0900

loqui (0.0.5-1) unstable; urgency=low

  * Initial Release.

 -- Yoichi Imai <yoichi@silver-forest.com>  Fri, 10 Jan 2003 21:02:09 +0900
